// for #3
db.rooms.insert({
    roomName: "single",
    roomCapacity: 2,
    roomPrice: 1000,
    roomDescription: "A simple room with all the basic necessities",
    roomAvailable: 10,
    isAvailable: false
});

// for # 4

db.rooms.insertMany([
    {
    roomName: "double",
    roomCapacity: 3,
    roomPrice: 2000,
    roomDescription: "A room fit for a small family going on a vacation",
    roomAvailable: 5,
    isAvailable: false
    },
    {
    roomName: "queen",
    roomCapacity: 4,
    roomPrice: 4000,
    roomDescription: "A room with a queen sized bed perfect for a simple getaway",
    roomAvailable: 15,
    isAvailable: false
    }
]);

// for # 5
db.rooms.find({roomName: "double"});

// for # 6

db.rooms.updateOne(
    {
        roomName: "queen"
    },
    {
        $set: {
            roomsAvailable: 0
        }
    }
    );

// for # 7
db.rooms.deleteMany({
    roomsAvailable: 0
});