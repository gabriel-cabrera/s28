/*MongoDB Operations*/
/*For Creating or Inserting data into database*/
db.users.insert({
    firstName: "Ely",
    lastName: "Buendia",
    age: 50,
    contact: {
        phone: "5347765",
        email: "ely@eraserheads.com"
        },
    courses: ["CSS", "JavaScript", "Python"],
    department: "none"
})

// for inserting many documents
db.users.insertMany([
	{
		firstName: "Chito",
		lastName: "Miranda",
		age: 43,
		contact: {
			phone: "5346234",
			email: "chito@parokya.com"
		},
		courses: ["Python", "React", "PHP"],
		department: "none"
	},
	{
		firstName: "Francis",
		lastName: "Magalona",
		age: 61,
		contact: {
			phone: "5347786",
			email: "francisM@email.com"
		},
		courses: ["React", "Laravel", "SASS"],
		department: "none"
	}
])

// =====================================
// for querying all the data in database
// for finding documents

// FIND ALL
db.users.find()

// FIND ONE document
db.users.find({firstName: "Francis", age: 61})

// =====================================

// For deleting data in database
//  for deleting one
db.users.deleteOne({
    firstName: "Ely"
    });

// ====================================
// for updating data in database
// for updating one
db.users.updateOne(
    {
        firstName: "Chito"
    },
    {   
        $set: {
            lastName: "Esguerra"
        }
    }
    );

// for updating many
db.users.updateMany(
    {
        department: "none"
    },
    {
        $set: {
            department: "HR"
        }
    }
);